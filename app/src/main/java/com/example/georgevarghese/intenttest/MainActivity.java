package com.example.georgevarghese.intenttest;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private String initialPayload = "upi://pay?am=1.00&pa=8277143608@apl&pn=Hemant&tn=Test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editText = findViewById(R.id.payload);
        editText.setText(initialPayload);

        findApps(initialPayload);
    }

    public void findApps(String payload) {
        PackageManager pm = getPackageManager();
        Intent upi_apps = new Intent();
        upi_apps.setData(Uri.parse(payload));

        final List<ResolveInfo> launchables = pm.queryIntentActivities(upi_apps, 0);
        Collections.sort(launchables, new ResolveInfo.DisplayNameComparator(pm));

        ListView listView = findViewById(R.id.app_list);
        final ArrayList<String> apps = new ArrayList<>();

        for (ResolveInfo resolveInfo : launchables) {
            try {
                ApplicationInfo ai = pm.getApplicationInfo(resolveInfo.activityInfo.packageName, 0);
                apps.add((String) pm.getApplicationLabel(ai));
            } catch(Exception e) {

            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, apps);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                launchApp(launchables.get(i));
            }
        });
    }

    public void launchApp(ResolveInfo resolveInfo) {
        EditText editText = findViewById(R.id.payload);
        String payload = editText.getEditableText().toString();

        Intent i = new Intent();
        i.setPackage(resolveInfo.activityInfo.packageName);
        i.setAction(Intent.ACTION_VIEW);
        i.setData(Uri.parse(payload));
        i.setFlags(Intent.EXTRA_DOCK_STATE_UNDOCKED);
        startActivityForResult(i, 1);
    }
}
